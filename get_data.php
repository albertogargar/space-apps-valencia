<?php 
error_reporting(E_ALL);
ini_set("display_errors", 1);
 
	if($_GET['hours']){
		$hours = $_GET['hours'];

		if($_GET['region']){
			$region = $_GET['region'];
		}
		$url = 'https://firms.modaps.eosdis.nasa.gov/active_fire/kml/';

		$filename = $region.'_'.$hours.'.kml';
		$url .= $filename;

		$path = 'files/';
		
		$ftoday = date("d-m-Y");

		if (file_exists($path.$filename)){
			$ffichero = date ("d-m-Y", filemtime($path.$filename));
			if ($ffichero == $ftoday){
				loadData($path.$filename);
				return;
			}else{
				unlink($path.$filename);
			}
		}

		$http_response = get_http_response_code($url);
		if($http_response != '404' && $http_response !== false){
			$file = file_get_contents($url);
			if (!file_exists($path)) { mkdir($path, 07777, true); }
			if (@copy($url, $path.$filename)) {
				loadData($path.$filename);
			}
		}else{
			print false;
		}

	}

	function loadData($route){
		$xml = simplexml_load_file($route);
		$childs2 = json_encode($xml,JSON_PRETTY_PRINT);
		print $childs2;
	}

	function get_http_response_code($url){
		try {

		    $headers = @get_headers($url);

	   		return substr($headers[0], 9, 3);


		} catch (Exception $e) {

		   	return false;
		}
	}
?>
