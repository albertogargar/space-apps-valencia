// Foundation JavaScript
// Documentation can be found at: http://foundation.zurb.com/docs
$(document).foundation();


$( document ).ready(function() {
    cargarRegiones();
    cargarHorarios();

});

function cargarRegiones(){
    var drop = document.getElementById("regiones");
    var regiones = ['Alaska','Australia','Canada','Central America','Europe','Northern and Central Africa','Russia and Asia','South America','South Asia',
    'South East Asia','Southern Africa','USA (Conterminous) and Hawaii'];
    for (var i = 0; i < regiones.length; i++) {
        var option = document.createElement("option");
        option.text = regiones[i];
        drop.add(option);
    };
}

function cargarHorarios(){
    var drop = document.getElementById("horarios");
    var horarios = ['24h','48h','animated_48h'];
    for (var i = 0; i < horarios.length; i++) {
        var option = document.createElement("option");
        option.text = horarios[i];
        drop.add(option);
    };
}

function cargarDatos(){
    var region = document.getElementById("regiones").value;
    region = region.replace(/\s+/g, '_');
    var hora = document.getElementById("horarios").value;
    $("#data").html('');
    $.ajax({
        type: "get",
        url: "http://localhost/spaceapps/get_data.php?hours="+hora+"&region="+region,
        success: function(data) {
            if (!data){
                $("#data").html('Ha sucedido un error.');
            }else{
                procesarDatos(data);
            }
        },
        error: function(xhr, status) {
            $("#data").html(status);
        }
    });

}

function procesarDatos(map_data){

    var styles = [{

      featureType : 'water',
    
      elementType : 'all',
    
      stylers : [{
        hue : '#ed1c24'
      }, {
        saturation : 73
      }, {
        lightness : -32
      }, {
        visibility : 'on'
      }]
    
    }, {
    
      featureType : 'landscape',
    
      elementType : 'all',
    
      stylers : [{
        hue : '#ed7f87'
      }, {
        saturation : 66
      }, {
        lightness : -20
      }, {
        visibility : 'simplified'
      }]
    
    }, {
    
      featureType : 'administrative',
    
      elementType : 'all',
    
      stylers : [{
        visibility : 'on'
      }]
    
    }, {
    
      featureType : 'administrative.locality',
    
      elementType : 'all',
    
      stylers : [{
        visibility : 'off'
      }]
    
    }];
    
    var options = {
    
      mapTypeControlOptions : {
    
        mapTypeIds : ['Styled']
    
      },
    
      center : new google.maps.LatLng(45.82880, -31.99219),
    
      zoom : 2,
    
      mapTypeId : 'Styled'
    
    };
    
    var div = document.getElementById('map');
    
    var map = new google.maps.Map(div, options);

    var styledMapType = new google.maps.StyledMapType(styles, {
      name : 'Styled'
    });

    map.mapTypes.set('Styled', styledMapType);
    
    map_data = JSON.parse(map_data);
    
    try {
        pointers = map_data['Document']['Folder']['Placemark'];
    }
    catch(err) {
        $("#data").html('El contenido del fichero es erroneo.');
        return;
    
    }
    
    for (var i in pointers){ 
        pointCoords = pointers[i]['Point']['coordinates'];
        coords = pointCoords.replace(/\s+/g, '');
        coords = pointCoords.split(",");
        
        var contentString = '<div id="content"><h1>'+pointers[i].name+'</h1></div>';
        var infowindow = new google.maps.InfoWindow({
            content: contentString
        });

        var marker = new google.maps.Marker({
            position: new google.maps.LatLng( coords[0],coords[1]),
            map: map,
            title: 'Hello World!'
        });

        google.maps.event.addListener(marker, 'click', function() {
          infowindow.open(map,marker);
        });





    }; 

 }
